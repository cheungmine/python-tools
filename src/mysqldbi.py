#!/usr/bin/python
#-*- coding: UTF-8 -*-
#
# @filename: mysqldbi.py
#
#
# @author: zhangliang 张亮
# @since: 2013-11-15
# @last: 2014-5-06
#
# @version: 0.1
#
# @usage:
#   dbi = MySQLDBInstance(host, port, db, user, passwd,
#       MySQLDBInstance.CHARSET_DEFAULT,
#       MySQLDBInstance.CURSOR_MODE_SSCURSOR)
#   dbi.connect()
#   # use dbi
#   ...
#   dbi.disconnect()
#
################################################################################
import os
import sys

import time
import datetime

import codecs

import optparse
import ConfigParser

import signal
import subprocess

import MySQLdb
import MySQLdb.cursors


class MySQLDBInstance:
    VERSION = "0.0.1"
    CHARSET_DEFAULT = "utf8"

    CURSOR_MODE_DEFAULT = 0
    CURSOR_MODE_SSCURSOR = 1
    CURSOR_MODE_DICTCURSOR = 2
    CURSOR_MODE_SSDICTCURSOR = 3

    def __init__(self, host, port, db, user, passwd, charset, mode):
        self.conn = None
        self.host = host
        self.port = int(port)
        self.db = db
        self.user = user
        self.passwd = passwd
        self.charset = charset
        self.curmode = mode
        self.cursors = {}


    def __del__(self):
        self.disconnect()

    def connected(self):
        if not self.conn:
            return False
        else:
            return True

    def connect(self):
        dbconn = None

        if not self.conn:
            if self.curmode == MySQLDBInstance.CURSOR_MODE_DEFAULT:
                try:
                    dbconn = MySQLdb.connect(
                        host = self.host,
                        port = self.port,
                        db = self.db,
                        user = self.user,
                        passwd = self.passwd,
                        charset = self.charset)
                except MySQLdb.Error, e:
                    print "MySQL DB Error %d: %s" % (e.args[0], e.args[1])
                finally:
                    self.conn = dbconn
            elif self.curmode == MySQLDBInstance.CURSOR_MODE_SSCURSOR:
                try:
                    dbconn = MySQLdb.connect(
                        host = self.host,
                        port = self.port,
                        db = self.db,
                        user = self.user,
                        passwd = self.passwd,
                        charset = self.charset,
                        cursorclass = MySQLdb.cursors.SSCursor)
                except MySQLdb.Error, e:
                    print "MySQL DB Error %d: %s" % (e.args[0], e.args[1])
                finally:
                    self.conn = dbconn
        else:
            print "WARNING: MySQL DB Connection is working"

        return self.conn


    def disconnect(self):
        if self.conn:
            try:
                for cursorName in self.cursors:
                    cursor = self.cursors[cursorName]
                    self.cursors[cursorName] = None
                    if cursor:
                        cursor.close()
                self.cursors.clear()
                self.conn.close()
            except MySQLdb.Error, e:
                print "MySQL DB Error %d: %s" % (e.args[0], e.args[1])
            finally:
                self.conn = None


    def getCursor(self, cursorName, cursorMode):
        cur = self.cursors.get(cursorName)
        if not cur:
            if not cursorMode:
                cur = self.conn.cursor()
            elif cursorMode == CURSOR_MODE_DEFAULT:
                cur = self.conn.cursor(MySQLdb.cursors.Cursor)
            elif cursorMode == CURSOR_MODE_SSCURSOR:
                cur = self.conn.cursor(MySQLdb.cursors.SSCursor)
            elif cursorMode == CURSOR_MODE_DICTCURSOR:
                cur = self.conn.cursor(MySQLdb.cursors.DictCursor)
            elif cursorMode == CURSOR_MODE_SSDICTCURSOR:
                cur = self.conn.cursor(MySQLdb.cursors.SSDictCursor)

            if cur:
                self.cursors[cursorName] = cur
        return cur


    def commit(self):
        if self.conn:
            self.conn.commit()


# test MySQLDBInstance
#
if __name__ == "__main__":
    host = "192.168.122.139"
    port = "3306"
    database = "report"
    user = "root"
    password = "root1234"
    encode = "utf8"

    dbi = MySQLDBInstance(
        host,
        port,
        database,
        user,
        password,
        encode,
        MySQLDBInstance.CURSOR_MODE_SSCURSOR
        )

    dbi.connect()
