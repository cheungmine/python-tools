delimiter |
DROP TABLE IF EXISTS test.`isam`;
CREATE TABLE test.`isam` (
    `BI`  BIGINT(20)             NOT NULL AUTO_INCREMENT,
    `TI`  TINYINT                DEFAULT NULL,
    `SI`  SMALLINT               DEFAULT NULL,
    `MI`  MEDIUMINT              DEFAULT NULL,
    `IT`  INT(11)                DEFAULT NULL,
    `IN`  INTEGER(8)             DEFAULT NULL,
    `FL`  FLOAT                  DEFAULT NULL,
    `DB`  DOUBLE                 DEFAULT NULL,
    `DE`  DECIMAL(14,4)          DEFAULT NULL,
    `DT`  DATETIME               DEFAULT NULL,
    `DA`  DATE                   DEFAULT NULL,
    `TM`  TIME                   DEFAULT NULL,
    `YR`  YEAR                   DEFAULT NULL,
    `TS`  TIMESTAMP              NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `CH`  CHAR(20)               DEFAULT NULL,
    `VC`  VARCHAR(300)           DEFAULT NULL,
    `TX`  TEXT                   DEFAULT NULL,
    `TT`  TINYTEXT               DEFAULT NULL,
    `MT`  MEDIUMTEXT             DEFAULT NULL,
    `LT`  LONGTEXT               DEFAULT NULL,
    `EN`  ENUM('MALE','FEMALE')  DEFAULT NULL,
    `ST`  SET('R','G','B','A')   DEFAULT NULL,
    `BA`  BINARY(16)             DEFAULT NULL,
    `VB`  VARBINARY(32)          DEFAULT NULL,
    `BT`  BIT(64)                DEFAULT NULL,
    `TB`  TINYBLOB               DEFAULT NULL,
    `BL`  BLOB                   DEFAULT NULL,
    `MB`  MEDIUMBLOB             DEFAULT NULL,
    `LB`  LONGBLOB               DEFAULT NULL,
    PRIMARY KEY `idx_isam_pk`  (`BI`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 |
delimiter ;


delimiter |
DROP TABLE IF EXISTS test.`inno`;
CREATE TABLE test.`inno` (
    `BI`  BIGINT(20)             NOT NULL AUTO_INCREMENT,
    `TI`  TINYINT                DEFAULT NULL,
    `SI`  SMALLINT               DEFAULT NULL,
    `MI`  MEDIUMINT              DEFAULT NULL,
    `IT`  INT(11)                DEFAULT NULL,
    `IN`  INTEGER(8)             DEFAULT NULL,
    `FL`  FLOAT                  DEFAULT NULL,
    `DB`  DOUBLE                 DEFAULT NULL,
    `DE`  DECIMAL(14,4)          DEFAULT NULL,
    `DT`  DATETIME               DEFAULT NULL,
    `DA`  DATE                   DEFAULT NULL,
    `TM`  TIME                   DEFAULT NULL,
    `YR`  YEAR                   DEFAULT NULL,
    `TS`  TIMESTAMP              NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `CH`  CHAR(20)               DEFAULT NULL,
    `VC`  VARCHAR(300)           DEFAULT NULL,
    `TX`  TEXT                   DEFAULT NULL,
    `TT`  TINYTEXT               DEFAULT NULL,
    `MT`  MEDIUMTEXT             DEFAULT NULL,
    `LT`  LONGTEXT               DEFAULT NULL,
    `EN`  ENUM('MALE','FEMALE')  DEFAULT NULL,
    `ST`  SET('R','G','B','A')   DEFAULT NULL,
    `BA`  BINARY(16)             DEFAULT NULL,
    `VB`  VARBINARY(32)          DEFAULT NULL,
    `BT`  BIT(64)                DEFAULT NULL,
    `TB`  TINYBLOB               DEFAULT NULL,
    `BL`  BLOB                   DEFAULT NULL,
    `MB`  MEDIUMBLOB             DEFAULT NULL,
    `LB`  LONGBLOB               DEFAULT NULL,
    PRIMARY KEY `idx_inno_pk`  (`BI`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 |
delimiter ;


delimiter |
DROP TABLE IF EXISTS test.`tmp`;
CREATE TABLE test.`tmp` (
    `BI`  BIGINT(20)             NOT NULL AUTO_INCREMENT,
    `TI`  TINYINT                DEFAULT NULL,
    `SI`  SMALLINT               DEFAULT NULL,
    `MI`  MEDIUMINT              DEFAULT NULL,
    `IT`  INT(11)                DEFAULT NULL,
    `IN`  INTEGER(8)             DEFAULT NULL,
    PRIMARY KEY `idx_tmp_pk`  (`BI`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 |
delimiter ;
