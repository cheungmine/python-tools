#!/usr/bin/python
#-*- coding: UTF-8 -*-
#
# @filename: mysqldb-test.py
#   mysql database test data
#   python multi-threads
#
# @author: zhangliang 张亮
# @since: 2014-5-06
# @date: 2014-5-07 13:07
# @version: 0.1
# @Usage:
#   $ python mysqldb-test.py
#
################################################################################
import threading
import datetime
import signal
import random

from ioutils import *
from mysqldbi import MySQLDBInstance

#----------------------------------------------------------
# change below settings if needed

# batch processing rows
BATCH_ROWS = 20

# process id
pid = os.getpid() 

# database login
host = "10.31.17.171"
port = "3306"
database = "test"
user = "car"
password = "abc123"
encode = "utf8"
table = 'test.tmp'

num_threads = 8
sleep_seconds = 60*60;
interval_seconds = 0.1

#==========================================================
# if exit app
#
global is_exit

# catch user press: ctrl+c
#
def sigint_handler(signo, frame):
    global is_exit
    is_exit = True
    print_warning("Process:%d receives an exit signal %d." % (pid, signo))


#==========================================================
# ThreadClass to operate on mysqldb
# This worker class is derived from the class threading.Thread
class worker(threading.Thread):
    def __init__(self, id, interval, proc, param, dbinst):
        threading.Thread.__init__(self)  
        self.thread_id = id
        self.interval = interval
        self.thread_proc = proc
        self.proc_param = param
        self.dbi = dbinst
        self.thread_stop = False

    # Overwrite run() method, put what you want the thread do here: 
    def run(self):
        self.dbi.connect()

        while not is_exit and not self.thread_stop and self.dbi.connected():
            self.thread_proc(self.thread_id, (self.proc_param, self.dbi))

            time.sleep(self.interval)
        else:
            if self.dbi.connected():
                self.dbi.disconnect()

    # Stop thread
    def stop(self):  
        self.thread_stop = True


#==========================================================
def start_threads(num_threads, interval, proc, param):
    threads = []

    for i in range(num_threads):
        dbi = MySQLDBInstance(host, port, database, user, password, encode, 0)
 
        thr = worker(i, interval, proc, param, dbi)
        thr.start()

        threads.append(thr)

    return threads


def stop_threads(threads):
    for i in range(len(threads)):
        threads[i].stop()
    return


#==========================================================
def testdb(thread_id, thread_param):
    (sql, dbi) = thread_param

    cur = dbi.getCursor("testdb"+str(thread_id), None)

    rows = []

    if sql.find('INSERT ') == 0:
        # insert clause:
        print_info("<thread: %d> INSERT %d rows" % (thread_id,BATCH_ROWS))
        for i in range(BATCH_ROWS):
            # create new row
            _row = (
                mkval('TI'),
                mkval('SI'),
                mkval('MI'),
                mkval('IT'),
                mkval('IN'),
                mkval('FL'),
                mkval('DB'),
                mkval('DE'),
                mkval('DT'),
                mkval('DA'),
                mkval('TM'),
                mkval('YR'),
                mkval('CH'),
                mkval('VC'),
                mkval('TX'),
                mkval('TT'),
                mkval('MT'),
                mkval('LT'),
                mkval('EN'),
                mkval('ST'),
                mkval('BA'),
                mkval('VB'),
                mkval('BT'),
                mkval('TB'),
                mkval('BL'),
                mkval('MB'),
                mkval('LB'))

            row = (
                mkval('TI'),
                mkval('SI'),
                mkval('MI'),
                mkval('IT'),
                mkval('IN'))
            # add row
            rows.append(row)
    elif sql.find('UPDATE ') == 0:
        # update clause:
        id = random.randint(1, 100000)
        print_warning("<thread: %d> UPDATE row=%d" % (thread_id,id))
        # create new row
        _row = (
            mkval('TI'),
            mkval('SI'),
            mkval('MI'),
            mkval('IT'),
            mkval('IN'),
            mkval('FL'),
            mkval('DB'),
            mkval('DE'),
            mkval('DT'),
            mkval('DA'),
            mkval('TM'),
            mkval('YR'),
            mkval('CH'),
            mkval('VC'),
            mkval('TX'),
            mkval('TT'),
            mkval('MT'),
            mkval('LT'),
            mkval('EN'),
            mkval('ST'),
            mkval('BA'),
            mkval('VB'),
            mkval('BT'),
            mkval('TB'),
            mkval('BL'),
            mkval('MB'),
            mkval('LB'),
            str(id)
        )

        row = (
            mkval('TI'),
            mkval('SI'),
            mkval('MI'),
            mkval('IT'),
            mkval('IN'),
            str(id)
        )
        # add row
        rows.append(row)
    elif sql.find('DELETE ') == 0:
        # delete clause:
        id = random.randint(1,100000)
        print_error("<thread: %d> DELETE row=%d" % (thread_id, id))
        # create new row
        row = (str(id))
        # add row
        rows.append(row)

    # execute sql:
    cur.executemany(sql, rows)
    dbi.commit()

    return


#==========================================================
# generate test data
def mkval(t):
    if t=='TI':
        return random.randint(0,127)
    elif t=='SI':
        #return random.randint(-32768,32767)
        return random.randint(0,127)
    elif t=='MI':
        #return random.randint(-8388608,8388607)
        return random.randint(0,127)
    elif t=='IT':
        #return random.randint(-2147483648,2147483647)
        return random.randint(0,127)
    elif t=='IN':
        #return random.randint(-2147483648,2147483647)
        return random.randint(0,127)
    elif t=='FL':
        #return random.uniform(-2147483648,2147483647)
        return random.randint(-128,127)
    elif t=='DB':
        #return random.uniform(-2147483648,2147483647)
        return random.randint(-128,127)
    elif t=='DE':
        #return random.uniform(-2147483648,2147483647)
        return random.randint(-128,127)
    elif t=='DT':
        return "str_to_date(date_format(now(), '%Y-%m-%d %H:%i:%S'), '%Y-%m-%d %H:%i:%S')"
    elif t=='DA':
        return "str_to_date(date_format(now(), '%Y-%m-%d'), '%Y-%m-%d')"
    elif t=='TM':
        return "str_to_date(time_format(now(), '%H:%i:%S'), '%H:%i:%S')"
    elif t=='YR':
        return random.randint(1901,2155)
    elif t=='CH':
        return 'NULL'
    elif t=='VC':
        return random.choice(['apple', 'pear', 'peach', 'orange', 'lemon'])
    elif t=='TX':
        return 'NULL'
    elif t=='TT':
        return 'NULL'
    elif t=='MT':
        return 'NULL'
    elif t=='LT':
        return 'NULL'
    elif t=='EN':
        return 'NULL'
    elif t=='ST':
        return 'NULL'
    elif t=='BA':
        return 'NULL'
    elif t=='VB':
        return 'NULL'
    elif t=='BT':
        return 'NULL'
    elif t=='TB':
        return 'NULL'
    elif t=='BL':
        return 'NULL'
    elif t=='MB':
        return 'NULL'
    elif t=='LB':
        return 'NULL'
    else:
        return 'NULL'


#==========================================================
# main() entry
if __name__ == "__main__":
    global is_exit
    is_exit = False

    print_info("mysqldb-test imitates inserting, updating or deleting randomly on mysql db.")

    signal.signal(signal.SIGINT, sigint_handler)  
    signal.signal(signal.SIGTERM, sigint_handler)

    # kill all zombie processes if have
    kill_process(sys.argv[0], pid)

    _insertSql = "INSERT INTO " + table + "("
    insertSql = "INSERT INTO " + table + "("
    _insertSql += '''
        `TI`,`SI`,`MI`,`IT`,`IN`,`FL`,`DB`,`DE`,`DT`,`DA`,`TM`,`YR`,`CH`,`VC`,
        `TX`,`TT`,`MT`,`LT`,`EN`,`ST`,`BA`,`VB`,`BT`,`TB`,`BL`,`MB`,`LB`
    ) VALUES (
        %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,
        %s,%s,%s,%s
    );'''

    # SI,MI>=0
    insertSql += '''
        `TI`,`SI`,`MI`,`IT`,`IN`
    ) VALUES (
        %s,%s,%s,%s,%s
    );'''

    _updateSql = "UPDATE " + table
    updateSql = "UPDATE " + table
    _updateSql += '''
        SET
            `TI`=%s,  `SI`=%s,  `MI`=%s,  `IT`=%s,  `IN`=%s,  `FL`=%s,  `DB`=%s,
            `DE`=%s,  `DT`=%s,  `DA`=%s,  `TM`=%s,  `YR`=%s,  `CH`=%s,  `VC`=%s,
            `TX`=%s,  `TT`=%s,  `MT`=%s,  `LT`=%s,  `EN`=%s,  `ST`=%s,  `BA`=%s,
            `VB`=%s,  `BT`=%s,  `TB`=%s,  `BL`=%s,  `MB`=%s,  `LB`=%s
        WHERE
            `BI`=%s;'''

    updateSql += '''
        SET
            `TI`=%s,  `SI`=%s,  `MI`=%s,  `IT`=%s,  `IN`=%s
        WHERE
            `BI`=%s;'''

    deleteSql = "DELETE FROM " + table + " WHERE `BI`=%s;"

    ins_threads = start_threads(num_threads, interval_seconds, testdb, insertSql)
    upd_threads = start_threads(num_threads, interval_seconds, testdb, updateSql)
    del_threads = start_threads(num_threads, interval_seconds, testdb, deleteSql)

    # wait 1 hours:
    time.sleep(sleep_seconds)

    # stop threads
    stop_threads(ins_threads)
    stop_threads(upd_threads)
    stop_threads(del_threads)
