#!/usr/bin/python
#-*- coding: UTF-8 -*-
#
# @filename: ioutils.py
#   file io functions
#
# @author: 
# @since: 2013-11-22
# @date:
# @version: 0.0.1
#   theVersion
#
################################################################################
import os
import sys
import time
import datetime
import shutil

import signal
import subprocess

import optparse
import fileinput

import getpass
import tempfile

import string
import codecs
import re

import optparse
import ConfigParser

codePage = "utf-8"
codePageNoSig = codePage

codePageWithSig = "utf-8-sig"

pid = os.getpid() 


# case sensitive ConfigParser
#
class CaseConfigParser(ConfigParser.ConfigParser):
    def optionxform(self, optionstr):
        return optionstr


# color output
#
def print_error(s):
    print '\033[31m[%d: ERROR] %s\033[31;m' % (pid, s)


def print_info(s):
    print '\033[32m[%d: INFO] %s\033[32;m' % (pid, s)


def print_warning(s):
    print '\033[33m[%d: WARNING] %s\033[33;m' % (pid, s)


# kill all processes by proc name
#
def kill_process(proc_name, not_pid):
    p = subprocess.Popen(['ps', '-ef'], stdout=subprocess.PIPE)
    out, err = p.communicate()
    pids = []
    pnames = []
    for line in out.splitlines():
        if proc_name in line:
            fields = line.split()
            if int(not_pid) != int(fields[1]):
                pids.append(int(fields[1]))
                pnames.append(" ".join(fields))

    for i in range(0, len(pids)):
        print_error("Kill zombie process (%d):\r\n---- %s ----" % (pids[i], pnames[i]))
        os.kill(pids[i], signal.SIGKILL)


# file io
#
def openFile(fname):
    fd = codecs.open(fname, "w+b", encoding = codePageNoSig)
    return fd


def writeFile(fd, str):
    fd.write(unicode(str, codePage))


def writelnFile(fd, str):
    fd.write(unicode(str, codePage) + '\r\n')


def writeln(fd):
    fd.write('\r\n')


def quot(str):
    return '"' + str + '"'


def closeFile(fd):
    fd.close()


def removeBomHeader(file):
    BOM = b'\xef\xbb\xbf'
    f = open(file, 'rb')
    if f.read(3) == BOM:
        fbody = f.read()
        f.close()
        with open(file, 'wb') as f:
            f.write(fbody)
    f.close()


def addBOMHeader(file):
    BOM = b'\xef\xbb\xbf'
    f = open(file, 'rb')
    if f.read(3) != BOM:
        f.close()
        f = open(file, 'rb')
        fbody = f.read()
        f.close()
        with open(file, 'wb') as f:
            f.write(BOM)
            f.write(fbody)
    f.close()


# create file and write to it
#
def createOutputFile(outfile, callbackWriteOutput, param):
    # create temp file
    tmpfd = tempfile.NamedTemporaryFile(delete = False)
    tmpfname = tmpfd.name
    tmpfd.close()
    try:
        if os.path.isfile(outfile):
            raise Exception(-10001, "File already exists")

        callbackWriteOutput(openFile(tmpfname), param)

        # create path if not exists
        floders = outfile.split('/')

        # remove last element: filename
        floders.pop()
        path = '/'.join(floders)

        # if path not exists, create it
        if not os.path.exists(path):
            os.makedirs(path)

        shutil.copy2(tmpfname, outfile)
        print_info("Create file: %s" % outfile)
    except Exception as e:
        print_error("createOutputFile() error: (%d) %s" % (e.args[0], e.args[1]))
        print_error("    file: %s" % outfile)
    finally:
        os.unlink(tmpfname)


# create config file and write to it
#
def createConfigFile(outfile, callbackConfigWrite, config):
    # create temp file
    tmpfd = tempfile.NamedTemporaryFile(delete = False)
    tmpfname = tmpfd.name
    tmpfd.close()
    try:
        if os.path.isfile(outfile):
            raise Exception(-10001, "File already exists")

        callbackConfigWrite(open(tmpfname, "w"), config)

        # create path if not exists
        floders = outfile.split('/')

        # remove last element: filename
        floders.pop()
        path = '/'.join(floders)

        # if path not exists, create it
        if not os.path.exists(path):
            os.makedirs(path)

        shutil.copy2(tmpfname, outfile)
        print_info("Create file: %s" % outfile)
    except Exception as e:
        print_error("createOutputFile() error: (%d) %s" % (e.args[0], e.args[1]))
        print_error("    file: %s" % outfile)
    finally:
        os.unlink(tmpfname)


# copy source file to dest file
#
def copyOutputFile(srcfile, destfile):
    try:
        if not os.path.isfile(srcfile):
            raise "File not found"

        if os.path.isfile(destfile):
            raise "File already existed"

        # create path if not exists
        floders = destfile.split('/')

        # remove last element: filename
        floders.pop()
        path = '/'.join(floders)

        # if path not exists, create it
        if not os.path.exists(path):
            os.makedirs(path)

        shutil.copy2(srcfile, destfile)
        print "[INFO] Copy to:", destfile
    except:
        print sys.exc_info()[0], sys.exc_info()[1]
        sys.exit("[EXCEPT] Error occurred when coping file")


# lower first letter
def lowerFirstLetter(inStr):
    tmpStr = inStr
    tmpStr = tmpStr.lower()
    outStr = tmpStr[0:1]
    tmpStr = inStr
    outStr += tmpStr[1:]
    return outStr


# uppper first letter
def upperFirstLetter(inStr):
    tmpStr = inStr
    tmpStr = tmpStr.upper()
    outStr = tmpStr[0:1]
    tmpStr = inStr
    outStr += tmpStr[1:]
    return outStr


class IncArrayList:
    data = []

    def __init__(self):
        self.data = []

    def __del__(self):
        self.data = []

    def clear(self):
        del self.data[0:]

    def length(self):
        return len(self.data)

    def set(self, i, item):
        while len(self.data) < i:
            self.data.append(None)
        self.data.append(item)

    def get(self, i):
        if len(self.data) > i:
            return self.data[i]
        else:
            return None
